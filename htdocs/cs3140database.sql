-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 09, 2019 at 06:38 PM
-- Server version: 8.0.18
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cs3140database`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `cID` int(10) UNSIGNED NOT NULL,
  `cpID` int(10) UNSIGNED NOT NULL,
  `ccomment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cauthor` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cauthemail` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cdateposted` datetime NOT NULL,
  `capproved` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `cusername` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `crevdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`cID`, `cpID`, `ccomment`, `cauthor`, `cauthemail`, `cdateposted`, `capproved`, `cusername`, `crevdate`) VALUES
(1, 2, 'comment on second blog', 'john smith', 'jsmith@aol.com', '2019-02-02 00:00:00', '1', 'jsmith', '2019-02-01 10:11:12'),
(2, 3, 'First comment on third blog', 'john smith', 'jsmith@aol.com', '2019-02-02 00:00:00', '1', 'jsmith', '2019-02-01 10:11:12'),
(3, 3, 'Second comment on third blog', 'john smith', 'jsmith@aol.com', '2019-03-02 00:00:00', '1', 'jsmith', '2019-02-02 10:11:12'),
(4, 4, 'First comment on fourth blog', 'john smith', 'jsmith@aol.com', '2019-04-01 00:00:00', '1', 'jsmith', '2019-02-02 10:11:12'),
(5, 4, 'Second comment on fourth blog', 'john smith', 'jsmith@aol.com', '2019-05-01 00:00:00', '1', 'jsmith', '2019-02-02 10:11:12'),
(6, 4, 'Third comment on fourth blog', 'john smith', 'jsmith@aol.com', '2019-06-01 00:00:00', '1', 'jsmith', '2019-02-02 10:11:12'),
(7, 5, 'First comment on fourth blog', 'john smith', 'jsmith@aol.com', '2019-01-01 00:00:00', '1', 'jsmith', '2019-02-02 10:11:12'),
(8, 5, 'Second comment on fourth blog', 'john smith', 'jsmith@aol.com', '2019-02-01 00:00:00', '1', 'jsmith', '2019-02-02 10:11:12'),
(9, 5, 'Third comment on fourth blog', 'john smith', 'jsmith@aol.com', '2019-03-01 00:00:00', '1', 'jsmith', '2019-02-02 10:11:12'),
(10, 5, 'Fourth comment on fourth blog', 'john smith', 'jsmith@aol.com', '2019-04-01 00:00:00', '1', 'jsmith', '2019-02-02 10:11:12'),
(11, 6, 'First comment on fourth blog', 'john smith', 'jsmith@aol.com', '2019-01-01 00:00:00', '1', 'jsmith', '2019-01-01 10:11:12'),
(13, 6, 'Third comment on fourth blog', 'john smith', 'jsmith@aol.com', '2019-03-01 00:00:00', '1', 'jsmith', '2019-01-03 10:11:12'),
(14, 6, 'Fourth comment on fourth blog', 'john smith', 'jsmith@aol.com', '2019-04-01 00:00:00', '1', 'jsmith', '2019-01-04 10:11:12'),
(15, 6, 'Fifth comment on fourth blog', 'john smith', 'jsmith@aol.com', '2019-05-01 00:00:00', '1', 'jsmith', '2019-01-05 10:11:12');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `pID` int(10) UNSIGNED NOT NULL,
  `pcontent` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `pheading` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `psubheading` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pkeywords` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pallowcomment` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '1',
  `pyear` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '2009',
  `pmonth` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '01',
  `pdateposted` datetime NOT NULL,
  `pauthor` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Default Name',
  `pusername` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `prevdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`pID`, `pcontent`, `pheading`, `psubheading`, `pkeywords`, `pallowcomment`, `pyear`, `pmonth`, `pdateposted`, `pauthor`, `pusername`, `prevdate`) VALUES
(1, 'Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed\r\n        tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.\r\n\r\n        Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed\r\n          tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.\r\n\r\n          Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed\r\n            tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.', 'TITLE heading', 'my first blog post', 'first, prime, uno', '1', '2019', '12', '2018-01-27 13:11:03', 'jared dukat', 'jdukat', '2018-01-25 12:12:12'),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices dui sapien eget mi proin sed libero enim. Aliquet nibh praesent tristique magna sit amet purus gravida. Ut ornare lectus sit amet est placerat in egestas. Tempus quam pellentesque nec nam. A scelerisque purus semper eget duis at tellus at urna. Bibendum enim facilisis gravida neque convallis a. Vitae nunc sed velit dignissim sodales ut eu. In hendrerit gravida rutrum quisque non tellus orci. Auctor urna nunc id cursus metus.\r\n\r\n    Condimentum id venenatis a condimentum vitae. Ornare suspendisse sed nisi lacus. Ultricies mi quis hendrerit dolor. Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Lorem donec massa sapien faucibus et molestie ac feugiat sed. Pulvinar mattis nunc sed blandit libero volutpat sed cras. Velit ut tortor pretium viverra suspendisse. Urna molestie at elementum eu facilisis sed odio. Parturient montes nascetur ridiculus mus mauris vitae ultricies leo. Etiam tempor orci eu lobortis elementum nibh tellus molestie nunc. Quis enim lobortis scelerisque fermentum dui. Iaculis nunc sed augue lacus viverra vitae congue eu consequat. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis. Tempus urna et pharetra pharetra massa massa ultricies mi quis. Adipiscing commodo elit at imperdiet. Vel risus commodo viverra maecenas accumsan lacus vel facilisis. Convallis convallis tellus id interdum velit laoreet.\r\n\r\n    Integer enim neque volutpat ac tincidunt vitae semper. Varius morbi enim nunc faucibus. Lectus arcu bibendum at varius vel pharetra vel turpis nunc. Leo a diam sollicitudin tempor id eu nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada. Amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar. Enim diam vulputate ut pharetra sit amet. Vel elit scelerisque mauris pellentesque. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Condimentum mattis pellentesque id nibh.', 'Second Blog', 'My second blog post', 'second, dos', '1', '2019', '01', '2019-01-01 00:00:00', 'Jared Dukat', 'admin', '2019-01-01 10:10:10'),
(3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices dui sapien eget mi proin sed libero enim. Aliquet nibh praesent tristique magna sit amet purus gravida. Ut ornare lectus sit amet est placerat in egestas. Tempus quam pellentesque nec nam. A scelerisque purus semper eget duis at tellus at urna. Bibendum enim facilisis gravida neque convallis a. Vitae nunc sed velit dignissim sodales ut eu. In hendrerit gravida rutrum quisque non tellus orci. Auctor urna nunc id cursus metus.\r\n\r\n 	 Condimentum id venenatis a condimentum vitae. Ornare suspendisse sed nisi lacus. Ultricies mi quis hendrerit dolor. Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Lorem donec massa sapien faucibus et molestie ac feugiat sed. Pulvinar mattis nunc sed blandit libero volutpat sed cras. Velit ut tortor pretium viverra suspendisse. Urna molestie at elementum eu facilisis sed odio. Parturient montes nascetur ridiculus mus mauris vitae ultricies leo. Etiam tempor orci eu lobortis elementum nibh tellus molestie nunc. Quis enim lobortis scelerisque fermentum dui. Iaculis nunc sed augue lacus viverra vitae congue eu consequat. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis. Tempus urna et pharetra pharetra massa massa ultricies mi quis. Adipiscing commodo elit at imperdiet. Vel risus commodo viverra maecenas accumsan lacus vel facilisis. Convallis convallis tellus id interdum velit laoreet.\r\n\r\n 	 Integer enim neque volutpat ac tincidunt vitae semper. Varius morbi enim nunc faucibus. Lectus arcu bibendum at varius vel pharetra vel turpis nunc. Leo a diam sollicitudin tempor id eu nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada. Amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar. Enim diam vulputate ut pharetra sit amet. Vel elit scelerisque mauris pellentesque. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Condimentum mattis pellentesque id nibh.', 'Third Blog', 'My third blog post', 'third, tres, three', '1', '2019', '03', '2019-03-03 00:00:00', 'Jared Dukat', 'admin', '2019-03-03 10:10:10'),
(4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices dui sapien eget mi proin sed libero enim. Aliquet nibh praesent tristique magna sit amet purus gravida. Ut ornare lectus sit amet est placerat in egestas. Tempus quam pellentesque nec nam. A scelerisque purus semper eget duis at tellus at urna. Bibendum enim facilisis gravida neque convallis a. Vitae nunc sed velit dignissim sodales ut eu. In hendrerit gravida rutrum quisque non tellus orci. Auctor urna nunc id cursus metus.\r\n\r\n 	 Condimentum id venenatis a condimentum vitae. Ornare suspendisse sed nisi lacus. Ultricies mi quis hendrerit dolor. Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Lorem donec massa sapien faucibus et molestie ac feugiat sed. Pulvinar mattis nunc sed blandit libero volutpat sed cras. Velit ut tortor pretium viverra suspendisse. Urna molestie at elementum eu facilisis sed odio. Parturient montes nascetur ridiculus mus mauris vitae ultricies leo. Etiam tempor orci eu lobortis elementum nibh tellus molestie nunc. Quis enim lobortis scelerisque fermentum dui. Iaculis nunc sed augue lacus viverra vitae congue eu consequat. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis. Tempus urna et pharetra pharetra massa massa ultricies mi quis. Adipiscing commodo elit at imperdiet. Vel risus commodo viverra maecenas accumsan lacus vel facilisis. Convallis convallis tellus id interdum velit laoreet.\r\n\r\n 	 Integer enim neque volutpat ac tincidunt vitae semper. Varius morbi enim nunc faucibus. Lectus arcu bibendum at varius vel pharetra vel turpis nunc. Leo a diam sollicitudin tempor id eu nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada. Amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar. Enim diam vulputate ut pharetra sit amet. Vel elit scelerisque mauris pellentesque. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Condimentum mattis pellentesque id nibh.', 'Fourth Blog', 'My fourth blog post', 'fourth, four', '1', '2019', '04', '2019-04-04 00:00:00', 'Jared Dukat', 'admin', '2019-04-04 10:10:10'),
(5, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices dui sapien eget mi proin sed libero enim. Aliquet nibh praesent tristique magna sit amet purus gravida. Ut ornare lectus sit amet est placerat in egestas. Tempus quam pellentesque nec nam. A scelerisque purus semper eget duis at tellus at urna. Bibendum enim facilisis gravida neque convallis a. Vitae nunc sed velit dignissim sodales ut eu. In hendrerit gravida rutrum quisque non tellus orci. Auctor urna nunc id cursus metus.\r\n\r\n 	 Condimentum id venenatis a condimentum vitae. Ornare suspendisse sed nisi lacus. Ultricies mi quis hendrerit dolor. Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Lorem donec massa sapien faucibus et molestie ac feugiat sed. Pulvinar mattis nunc sed blandit libero volutpat sed cras. Velit ut tortor pretium viverra suspendisse. Urna molestie at elementum eu facilisis sed odio. Parturient montes nascetur ridiculus mus mauris vitae ultricies leo. Etiam tempor orci eu lobortis elementum nibh tellus molestie nunc. Quis enim lobortis scelerisque fermentum dui. Iaculis nunc sed augue lacus viverra vitae congue eu consequat. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis. Tempus urna et pharetra pharetra massa massa ultricies mi quis. Adipiscing commodo elit at imperdiet. Vel risus commodo viverra maecenas accumsan lacus vel facilisis. Convallis convallis tellus id interdum velit laoreet.\r\n\r\n 	 Integer enim neque volutpat ac tincidunt vitae semper. Varius morbi enim nunc faucibus. Lectus arcu bibendum at varius vel pharetra vel turpis nunc. Leo a diam sollicitudin tempor id eu nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada. Amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar. Enim diam vulputate ut pharetra sit amet. Vel elit scelerisque mauris pellentesque. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Condimentum mattis pellentesque id nibh.', 'Fifth Blog', 'My fifth blog post', 'fifth, five', '1', '2019', '05', '2019-05-05 00:00:00', 'Jared Dukat', 'admin', '2019-05-05 10:10:10'),
(6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices dui sapien eget mi proin sed libero enim. Aliquet nibh praesent tristique magna sit amet purus gravida. Ut ornare lectus sit amet est placerat in egestas. Tempus quam pellentesque nec nam. A scelerisque purus semper eget duis at tellus at urna. Bibendum enim facilisis gravida neque convallis a. Vitae nunc sed velit dignissim sodales ut eu. In hendrerit gravida rutrum quisque non tellus orci. Auctor urna nunc id cursus metus.\r\n\r\n 	 Condimentum id venenatis a condimentum vitae. Ornare suspendisse sed nisi lacus. Ultricies mi quis hendrerit dolor. Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Lorem donec massa sapien faucibus et molestie ac feugiat sed. Pulvinar mattis nunc sed blandit libero volutpat sed cras. Velit ut tortor pretium viverra suspendisse. Urna molestie at elementum eu facilisis sed odio. Parturient montes nascetur ridiculus mus mauris vitae ultricies leo. Etiam tempor orci eu lobortis elementum nibh tellus molestie nunc. Quis enim lobortis scelerisque fermentum dui. Iaculis nunc sed augue lacus viverra vitae congue eu consequat. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis. Tempus urna et pharetra pharetra massa massa ultricies mi quis. Adipiscing commodo elit at imperdiet. Vel risus commodo viverra maecenas accumsan lacus vel facilisis. Convallis convallis tellus id interdum velit laoreet.\r\n\r\n 	 Integer enim neque volutpat ac tincidunt vitae semper. Varius morbi enim nunc faucibus. Lectus arcu bibendum at varius vel pharetra vel turpis nunc. Leo a diam sollicitudin tempor id eu nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada. Amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar. Enim diam vulputate ut pharetra sit amet. Vel elit scelerisque mauris pellentesque. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Condimentum mattis pellentesque id nibh.', 'Sixth Blog', 'My sixth blog post', 'sixth, six', '1', '2019', '06', '2019-06-06 00:00:00', 'Jared Dukat', 'admin', '2019-06-06 10:10:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`cID`);
ALTER TABLE `comments` ADD FULLTEXT KEY `ccomment` (`ccomment`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`pID`);
ALTER TABLE `posts` ADD FULLTEXT KEY `pheading` (`pheading`,`pkeywords`);
ALTER TABLE `posts` ADD FULLTEXT KEY `pcontent` (`pcontent`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `cID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `pID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
